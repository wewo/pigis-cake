<div class="row">
	<div class="col-xs-12 col-md-5">

	<?php

	echo $this->Form->create('Newsletter', array(
		'id' => 'newsletter-form'
	));

	echo '<div class="row">';
	echo '<div class="col-xs-12 col-sm-8">';

		echo '<label for="NewsletterDate">Dátum</label>';
		echo $this->Form->input('Newsletter.date', array(
			'type' => 'text',
			'class' => 'form-control',
			'label' => false,
			'default' => getTodayDateFormatted(),
			'div' => array('class' => 'input input-group date '),
			'after' => '<div class="input-group-addon"><span class="glyphicon glyphicon-th"></span></div>'
		));

	echo '</div></div>';
	echo '<div class="row">';
	echo '<div class="col-xs-12 col-sm-8">';

		echo $this->Form->input('Newsletter.opening_time', array(
			'type' => 'text',
			'default' => false,
			'class' => 'form-control',
			'default' => $newsletter_data['opening_time'],
			'label' => 'Otváracia doba'
		));

	echo '</div></div>';
	echo '<div class="row">';
	echo '<div class="col-xs-12 col-md-10">';

		echo $this->Form->input('Newsletter.image_url', array(
			'type' => 'url',
			'default' => $newsletter_data['image_url'],
			'class' => 'form-control',
			'label' => 'URL obrázka'
		));

	echo '</div></div>';
	echo '<div class="row">';
	echo '<div class="col-xs-12 col-md-10">';

		echo $this->Form->input('Newsletter.image_alt', array(
			'type' => 'textarea',
			'default' => $newsletter_data['image_alt'],
			'class' => 'form-control',
			'label' => 'ALT obrázka (prepísaný text)'
		));

	echo '</div></div>';

	echo $this->Form->submit('Uložiť', array(
		'class' => 'btn btn-primary',
		'style' => 'min-width: 85px'
	));

	echo '<br>';

	echo $this->Html->link('Upraviť denné menu', '/admin', array(
		'class' => 'btn btn-default',
		'style' => 'min-width: 85px'
	));

	echo $this->Form->end();
	echo '<br><br><br>';
	?>

	<div class="row">
		<div class="col-xs-12 col-md-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Prihlásení do newsletteru (<?php echo sizeof($newsletter_addresses_list); ?>)</h3>
				</div>
				<div class="panel-body">
				<?php
				foreach ($newsletter_addresses_list as $email) {
					echo sprintf('<p>%s</p>', $email);
				}
				?>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-4">
			<?php
			echo $this->Html->link('Odoslať newsletter', '#', array(
				'class' => 'btn btn-primary',
				'id' => 'send-newsletter-button',
				'style' => 'padding-top:0px;padding-bottom:0px;line-height: 38px'
			));
			?>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-md-8">
			<p id="send-newsletter-success">Newsletter bol odoslaný.</p>
		</div>
	</div>

	<br><br>

	</div>
	<div class="col-xs-12 col-md-7">
		<label>Náhľad</label>
		<div style="max-width:100%;overflow-x:scroll">
			<?php echo $this->Element('daily_offer_mail'); ?>
		</div>
	</div>
</div>

<?php
	function getTodayDateFormatted() {
		$localized = array(
			'January' => 'Január',
			'February' => 'Február',
			'March' => 'Marec',
			'April' => 'Apríl',
			'May' => 'Máj',
			'June' => 'Jún',
			'July' => 'Júl',
			'August' => 'August',
			'September' => 'September',
			'October' => 'Október',
			'November' => 'November',
			'December' => 'December'
		);

		return date("j").'. '.$localized[date("F")].' '.date("Y");
	}
?>
