<div class="row">
	<div class="col-xs-12 col-md-6">			
		<?php
		echo $this->element('admin/daily_offer_form', array(
			'form_name' => 'SK_DailyOffer',
			'daily_offer_id' => SK_DAILY_OFFER_ID,
			'data' => $sk_daily_menu_data
		));
		?>
	</div>

	<?php if(0): ?>
	<div class="col-xs-12 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">EN Denné menu</h3>
			</div>
			<div class="panel-body">
				<?php
				echo $this->element('admin/daily_offer_form', array(
					'form_name' => 'EN_DailyOffer',
					'daily_offer_id' => EN_DAILY_OFFER_ID,
					'data' => $en_daily_menu_data
				));
				?>
			</div>
		</div>
	</div>
	<?php endif; ?>
</div>

<?php if(0): ?>
<div class="row">
	<div class="col-xs-12 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">SK Špeciality</h3>
			</div>
			<div class="panel-body">
				<?php
				echo $this->Form->create('SK_Specials', array(
					'class' => 'specials-form'
				));
				?>

				<div class="row">
					<div class="col-xs-12">
						<?php
						foreach ($config_values as $config_value) {
							if( $config_value['ConfigValue']['lang'] == 'sk' && $config_value['ConfigValue']['name'] == 'specials_name') {
								echo $this->Form->input('ConfigValue.specials_name', array(
									'default' => $config_value['ConfigValue']['value'],
									'class' => 'form-control specials-name',
									'label' => __('Názov'),
									'data-config-val-id' => $config_value['ConfigValue']['id']
								));
							}
						}
						?>
					</div>
				</div>

				<?php
				foreach ($specials_data as $special_data) {
					if( $special_data['Special']['lang'] == 'sk' ) {
						echo $this->element('admin/special_form_block', $special_data['Special']);
					}
				}

				echo $this->Form->submit('Uložiť', array(
					'class' => 'btn btn-primary'
				));

				echo $this->Form->end();
				?>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">EN Špeciality</h3>
			</div>
			<div class="panel-body">
				<?php
				echo $this->Form->create('EN_Specials', array(
					'class' => 'specials-form'
				));
				?>

				<div class="row">
					<div class="col-xs-12">
						<?php

						foreach ($config_values as $config_value) {
							if( $config_value['ConfigValue']['lang'] == 'en' && $config_value['ConfigValue']['name'] == 'specials_name') {

								echo $this->Form->input('ConfigValue.specials_name', array(
									'default' => $config_value['ConfigValue']['value'],
									'class' => 'form-control specials-name',
									'label' => __('Názov'),
									'data-config-val-id' => $config_value['ConfigValue']['id']
								));
							}
						}
						?>
					</div>
				</div>

				<?php
				foreach ($specials_data as $special_data) {
					if( $special_data['Special']['lang'] == 'en' ) {
						echo $this->element('admin/special_form_block', $special_data['Special']);
					}
				}

				echo $this->Form->submit('Uložiť', array(
					'class' => 'btn btn-primary'
				));

				echo $this->Form->end();
				?>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
