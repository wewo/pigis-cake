<div class="wrapper wrapper-fullscreen wrapper-image" id="uvod">
	<div id="uvod-text" class="hidden-xs">
		<div class="green"><?php echo __('Toto je Pigis'); ?></div>
		<div class="white"><?php echo __('lokálne bistro'); ?></div>
	</div>
	<a href="#" id="scroll-down" onclick="scrollToElement('#kuchar'); return false;"></a>
</div>

<div class="wrapper" id="kuchar">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<blockquote>
					<p>“<?php echo __('Zaručujem kvalitu surovín od lokálnych farmárov.'); ?>”</p>
					<cite><strong>Pavol Búci</strong> — <?php echo __('šéfkuchár'); ?></cite>
				</blockquote>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<div id="kuchar-img-text-holder">
					<div id="kuchar-img"></div>
					<div id="kuchar-text">
						<h2><?php echo __('To najlepšie z Bystrici'); ?></h2>
						<h3><?php echo __('100% čerstvé suroviny od lokálnych farmárov'); ?></h3>
						<p><?php echo __('Naše bistro Pigis je výnimočné hlavne zameraním sa na sezónnosť, poctivosť a domáce produkty. Každý deň spolupracujeme s lokálnymi farmármi a pestovateľmi. Poznáme ich osobne a preto zaručujeme kvalitu ich produktov.'); ?></p>
						<p><?php echo __('Ak máte záujem o rezerváciu rodinnej oslavy či business stretnutia v našej reštaurácii, radi vám pripravíme aj jedlá, či degustačné menu, ktoré nie sú súčasťou aktuálnej ponuky.'); ?></p>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-6 col-md-3">
					<div class="circle-link" id="chlieb"><?php echo __('Pečieme<br>domáci chlieb'); ?></div>
				</div>
				<div class="col-xs-6 col-md-3">
					<div class="circle-link" id="kolace"><?php echo __('Pripravujeme<br>vlastné koláče'); ?></div>
				</div>
				<div class="col-xs-6 col-md-3">
					<div class="circle-link" id="kava"><?php echo __('Melieme kávu<br>z lokálnej pražiarne'); ?></div>
				</div>
				<div class="col-xs-6 col-md-3">
					<div class="circle-link" id="zemiaciky"><?php echo __('Podávame<br>domáce zemiačky'); ?></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="wrapper wrapper-image" id="burger">
	<div></div>
	<h2><?php echo __('Špeciality dňa'); ?></h2>
</div>

<div class="wrapper wrapper-image" id="denne-menu">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2><?php // echo __('Denná ponuka'); ?></h2>
				<p class="description"><?php echo __('v pracovné dni od 14.30 do 15.30 a v nedeľu nevaríme'); ?></p>

				<?php
				$daily_offer_visible = 'false';
				foreach ($config_values as $config_value) {
					if( $config_value['ConfigValue']['name'] == 'daily_offer_visible') {
							$daily_offer_visible = $config_value['ConfigValue']['value'];
					}
				}
				?>


				<?php
				if( $daily_offer_visible === 'true' ) {
					echo $this->element('daily_menu', $daily_menu_data['DailyOffer']);
				}
				else {
					echo __('Špeciality dňa ešte neboli zverejnené.');
				}
				?>
			</div>

			<?php if(0): ?>
			<div class="col-xs-12 col-md-6" id="specials">
				<h2><?php
				foreach ($config_values as $config_value) {
					if( $config_value['ConfigValue']['name'] == 'specials_name') {
							echo $config_value['ConfigValue']['value'];
					}
				}
				?></h2>
				<p class="description"><?php echo __('pracovné dni od 15.30 do 21.00,<br> sobota od 11.30 do 21.00'); ?></p>

				<?php
				foreach ($specials_data as $special) {
					echo $this->element('special', $special['Special']);
				}
				?>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>

<div class="wrapper wrapper-image" id="green-lines">
	<form action="#">
		<span><?php echo __('Dostávajte dennú ponuku na svoj e-mail'); ?></span>
		<input type="email" placeholder="<?php echo __('váš e-mail'); ?>" />
		<input type="submit" value="<?php echo __('Odoslať'); ?>" />
	</form>
	<p id="newsletter-success"><?php echo __('Prihlásenie do newsletteru bolo úspešné. Ďakujeme.'); ?></p>
	<p id="newsletter-exists"><?php echo __('Táto adresa je už prihlásená.'); ?>&nbsp;<a href="#" class="newsletter-try-again"><?php echo __('Skúsiť znova'); ?></a></p>
	<p id="newsletter-error"><?php echo __('Nastala chyba. Skúste sa prosím <a href="#" class="newsletter-try-again">prihlásiť znova</a>.'); ?><?php echo __('Opakovať'); ?></p>
</div>

<div class="wrapper" id="events">
	<div class="container-fluid">
		<?php
			foreach ($events_data as $event_data) {
				echo $this->element('event-block', $event_data['Event']);
			}
		?>
	</div>
</div>

<div id="gallery" class="owl-carousel owl-theme">
	<?php echo $this->element('gallery', $gallery_image_file_names); ?>
</div>

<div class="wrapper wrapper-image" id="makronkovo">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-offset-6 col-md-6">
				<img src="img/logos/markonkovo.svg" alt="Makrónkovo - logo" width="380" height="107">
				<h2><?php echo __('Ručná výroba makróniek a koláčov'); ?></h2>
				<div class="row">
					<div class="col-xs-12 col-md-6">
						<h3><?php echo __('Candy bar'); ?></h3>
						<p><?php echo __('Svadba, oslava, event... tam všade nesmú chýbať naše makrónky a koláčiky.'); ?></p>
						<p><?php echo __('Pripravíme pre vás krásny stolík s dobrotami, ktorý zladíme s vašou výzdobou.'); ?></p>
						<a href="#" onclick="return false;">0948 009 800</a>
					</div>
					<div class="col-xs-12 col-md-6">
						<h3><?php echo __('Makrónky a koláče'); ?></h3>
						<p><?php echo __('Ponúkame vlastnoručne vyrábané makrónky a koláče najrôznejších delikátnych chutí.'); ?></p>
						<p><?php echo __('Môžete ich ochutnať v bistre Pigis pri kávičke alebo vám ich pripravíme na domácu oslavu.'); ?></p>
						<a href="http://fb.com/makronkovo" target="_blank">fb.com/markonkovo</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<footer class="wrapper wrapper-image" id="kontakt">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-offset-1 col-md-10">
				<div class="row ">
					<div class="col-xs-12">
						<img id="footer-logo" src="/img/logos/pigis-text-logo-white.svg" alt="Pigis logo" width="180" height="91">
						<br><br>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-4">
						<div class="footer-block">
							<h2><?php echo __('Adresa'); ?></h2>
							<address>Dolná 50<br>974 01 Banská Bystrica</address>
						</div>
						<div class="footer-block wider">
							<h2><?php echo __('Rezervácie'); ?></h2>
							<p>+421 949 167 367, <a class="mail-link" href="mailto:pigis@pigis.sk">pigis@pigis.sk</a></p>
						</div>
					</div>

					<div class="col-xs-12 col-md-4">
						<div class="footer-block">
							<h2><?php echo __('Otváracie hodiny'); ?></h2>
							<table>
								<tr>
									<td><?php echo __('Pondelok'); ?></td>
									<td>8.00 - 22.00</td>
								</tr>
								<tr>
									<td><?php echo __('Utorok'); ?></td>
									<td>8.00 - 22.00</td>
								</tr>
								<tr>
									<td><?php echo __('Streda'); ?></td>
									<td>8.00 - 22.00</td>
								</tr>
								<tr>
									<td><?php echo __('Štvrtok'); ?></td>
									<td>8.00 - 22.00</td>
								</tr>
								<tr>
									<td><?php echo __('Piatok'); ?></td>
									<td>8.00 - 23.00</td>
								</tr>
								<tr>
									<td><?php echo __('Sobota'); ?></td>
									<td>11.00 - 23.00</td>
								</tr>
								<tr>
									<td><?php echo __('Nedeľa'); ?></td>
									<td>11.00 - 20.00</td>
								</tr>
							</table>
							<p class="hidden-md hidden-lg" style="color:#999"><?php echo __('V čase od 14.30 do 15.30<br>je kuchyňa zatvorená.'); ?></p>
						</div>
					</div>

					<div class="col-xs-12 col-md-4">
						<div class="footer-block">
							<h2><?php echo __('Sociálne médiá'); ?></h2>
							<br>
							<a target="_blank" class="icon-link" href="https://www.facebook.com/Pigis">
								<svg alt="Facebook page" width="30" height="30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 266.9 266.9" enable-background="new 0 0 266.9 266.9" xml:space="preserve">
									<path d="M252.2,0H14.7C6.6,0,0,6.6,0,14.7v237.4c0,8.1,6.6,14.7,14.7,14.7h127.8V163.5h-34.8v-40.3
									h34.8V93.6c0-34.5,21.1-53.2,51.8-53.2c14.7,0,27.4,1.1,31.1,1.6v36l-21.3,0c-16.7,0-20,7.9-20,19.6v25.7H224l-5.2,40.3h-34.7v103.4
									h68c8.1,0,14.7-6.6,14.7-14.7V14.7C266.9,6.6,260.3,0,252.2,0z"/>
								</svg>
							</a>
							<a target="_blank" class="icon-link" href="https://www.instagram.com/pigis_bistro/">
								<svg alt="Instagram account" width="30" height="30" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 49.6 49.6" enable-background="new 0 0 49.6 49.6" xml:space="preserve">
									<path d="M6.4,0h36.9c3.5,0,6.4,2.9,6.4,6.4v36.9c0,3.5-2.9,6.4-6.4,6.4H6.4c-3.5,0-6.4-2.9-6.4-6.4V6.4
									C0,2.9,2.9,0,6.4,0z M36.2,5.5c-1.2,0-2.2,1-2.2,2.2v5.3c0,1.2,1,2.2,2.2,2.2h5.6c1.2,0,2.2-1,2.2-2.2V7.8c0-1.2-1-2.2-2.2-2.2H36.2
									z M44,21h-4.4c0.4,1.4,0.6,2.8,0.6,4.3c0,8.3-6.9,14.9-15.4,14.9c-8.5,0-15.4-6.7-15.4-14.9c0-1.5,0.2-2.9,0.6-4.3H5.5v21
									c0,1.1,0.9,2,2,2h34.6c1.1,0,2-0.9,2-2V21z M24.9,15.1c-5.5,0-10,4.3-10,9.7c0,5.3,4.5,9.7,10,9.7c5.5,0,10-4.3,10-9.7
									C34.8,19.4,30.4,15.1,24.9,15.1z"/>
								</svg>
							</a>
							<a target="_blank" class="icon-link" href="https://foursquare.com/v/pigis/4f3cb6bde4b00758870d0332">
								<svg alt="Foursquare" width="30" height="30" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 109.5 152.9" enable-background="new 0 0 109.5 152.9" xml:space="preserve">
									<path d="M96.1,0c0,0-70.3,0-81.5,0C3.3,0,0,8.5,0,13.8c0,5.3,0,129.5,0,129.5c0,6,3.2,8.2,5,9
									c1.8,0.7,6.8,1.4,9.8-2.1c0,0,38.5-44.7,39.2-45.3c1-1,1-1,2-1c2,0,16.8,0,24.9,0c10.5,0,12.1-7.5,13.2-11.9
									c0.9-3.7,11.1-55.9,14.5-72.5C111.3,6.8,108.1,0,96.1,0z M94.1,92c0.9-3.7,11.1-55.9,14.5-72.5 M91.3,22.1l-3.4,17.8
									c-0.4,1.9-2.8,4-5.1,4c-2.3,0-31.8,0-31.8,0c-3.6,0-6.1,2.4-6.1,6v3.9c0,3.6,2.6,6.1,6.1,6.1c0,0,24.4,0,26.9,0
									c2.5,0,5,2.8,4.5,5.5C81.9,68,79.3,81.4,79,82.9c-0.3,1.5-2,4-5,4c-2.5,0-22,0-22,0c-4,0-5.2,0.5-7.9,3.9
									C41.4,94,17.3,123,17.3,123c-0.2,0.3-0.5,0.2-0.5-0.1V21.8c0-2.3,2-5,5-5c0,0,62.9,0,65.4,0C89.6,16.8,91.9,19.1,91.3,22.1z"/>
								</svg>
							</a>
							<a target="_blank" class="icon-link" href="https://www.tripadvisor.sk/Restaurant_Review-g274923-d4505799-Reviews-Pigis-Banska_Bystrica_Banska_Bystrica_Region.html">
							<svg width="30" height="30" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 141.9 83.3" enable-background="new 0 0 141.9 83.3" xml:space="preserve">
								<g>
									<path d="M34.3,42.2c-3.2,0-5.8,2.6-5.8,5.8c0,3.2,2.6,5.8,5.8,5.8s5.8-2.6,5.8-5.8C40.1,44.7,37.5,42.2,34.3,42.2z"
										/>
									<path d="M105.4,42.1c-3.2,0-5.8,2.6-5.8,5.8s2.6,5.8,5.8,5.8c3.2,0,5.8-2.6,5.8-5.8S108.6,42.1,105.4,42.1z"/>
									<g>
										<path d="M35.2,30.8c-9.5,0-17.1,7.7-17.1,17.2c0,9.5,7.7,17.1,17.1,17.1c9.5,0,17.2-7.7,17.2-17.1
											C52.4,38.4,44.7,30.8,35.2,30.8z M35.2,59.8c-6.6,0-11.9-5.3-11.9-11.9c0-6.6,5.3-11.9,11.9-11.9c6.6,0,11.9,5.3,11.9,11.9
											C47.1,54.5,41.8,59.8,35.2,59.8z M141.9,13.2h-23.6C106.3,5.1,89.8,0,71,0C52.1,0,34.5,5.1,22.5,13.2H0c3.7,4.3,6.4,10.1,7.1,14.1
											c-4.2,5.8-6.8,13-6.8,20.7c0,19.5,15.8,35.3,35.3,35.3c11.1,0,21-5.1,27.4-13.1c2.6,3.1,7,9.3,7.9,11.1c0,0,5.1-7.6,7.9-11.1
											c6.5,8,16.3,13,27.4,13c19.5,0,35.3-15.8,35.3-35.3c0-7.8-2.5-14.9-6.8-20.7C135.6,23.3,138.3,17.5,141.9,13.2z M35.6,76.4
											C19.9,76.4,7.2,63.7,7.2,48c0-15.7,12.7-28.4,28.4-28.4S64.1,32.3,64.1,48C64.1,63.7,51.3,76.4,35.6,76.4z M35.7,12.7
											C45.8,8.3,58.4,5.9,71,5.9c12.6,0,24.1,2.4,34.2,6.8C86.2,13.3,71,28.9,71,48C71,28.5,55.2,12.7,35.7,12.7z M106.3,76.4
											c-15.7,0-28.4-12.7-28.4-28.4c0-15.7,12.7-28.4,28.4-28.4c15.7,0,28.4,12.7,28.4,28.4C134.7,63.7,122,76.4,106.3,76.4z
											 M106.3,30.7c-9.5,0-17.1,7.7-17.1,17.2c0,9.5,7.7,17.1,17.1,17.1c9.5,0,17.1-7.7,17.1-17.1C123.4,38.4,115.8,30.7,106.3,30.7z
											 M106.3,59.7c-6.6,0-11.9-5.3-11.9-11.9c0-6.6,5.3-11.9,11.9-11.9s11.9,5.3,11.9,11.9C118.2,54.4,112.9,59.7,106.3,59.7z"/>
									</g>
								</g>
							</svg>
							</a>
							<br><br>
							<!-- <div class="fb-like" data-layout="button" data-share="false" data-width="47" data-show-faces="false"></div> -->
							<br><br>
							<p class="hidden-xs hidden-sm" style="color:#999"><?php echo __('V pracovné dni v čase od 14.30 do 15.30<br> a v nedeľu je kuchyňa zatvorená.'); ?></p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12">
				<p style="color:#666; text-align:center;">Copyright 2015 Pigis.<br>Design and photo by <a style="color:#666; text-decoration:underline;" target="_blank" href="http://www.mealujemto.sk/">Michal Hornicky</a>. Code by <a style="color:#666; text-decoration:underline;" target="_blank" href="http://www.matusbielik.sk/">Matus Bielik</a>.</p>
			</div>
		</div>
	</div>
</footer>
