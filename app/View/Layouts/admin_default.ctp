<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

// $cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
// $cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $pageTitle; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('admin/sb-admin');
		echo $this->Html->css('plugins/morris');
		echo $this->Html->css('/font-awesome/css/font-awesome.min');
		echo $this->Html->css('admin/styles');
		echo $this->Html->css('admin/bootstrap-datepicker3.min');

		echo $this->Html->script('jquery');
		echo $this->Html->script('bootstrap.min');
		echo $this->Html->script('admin/script');
		echo $this->Html->script('admin/bootstrap-datepicker.min');
		echo $this->Html->script('admin/bootstrap-datepicker.sk.min');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<div id="wrapper">
		
		<div id="page-wrapper">

		<?php echo $this->element('/admin/menu'); ?>

			<div class="container-fluid">
				<?php echo $this->Session->flash(); ?>

				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header"><?php echo $pageTitle; ?><br><small><?php echo $pageSubtitle; ?></small></h1>
					</div>
				</div>

				<?php echo $this->fetch('content'); ?>

			</div>
		</div>
	</div>
	<?php // echo $this->element('sql_dump'); ?>
</body>
</html>
