<?php if( !empty($text) && !empty($price1) ): ?>

<p class="meal"><?php echo $text; ?><br>
	<span class="weight"><?php echo $weight1; ?></span> <span class="price"><?php echo $price1; ?>&euro;</span> <?php if (!empty($weight2) && !empty($price2)): ?>/ <span class="weight"><?php echo $weight2; ?></span> <span class="price"><?php echo $price2; ?>&euro;</span><?php endif; ?>
</p>

<?php endif; ?>