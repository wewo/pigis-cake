<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<?php echo $this->Html->link('Pigis Admin', '/', array('class'=>'navbar-brand')); ?>
	</div>

	<?php if( !empty($logged) ): ?>
	<!-- Top Menu Items -->
	<ul class="nav navbar-right top-nav">
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-user"></i> <?php echo $logged['name']; ?> <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<li>
					<?php
					echo $this->Html->link(
						'<i class="fa fa-fw fa-power-off"></i> '.__('Odhlásiť sa'),
						array(
							'controller' => 'users',
							'action' => 'logout'
						),
						array( 'escape' => false )
					);
					?>
				</li>
			</ul>
		</li>
	</ul>
	
	<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul class="nav navbar-nav side-nav">
			<li id="home-menu-item">
				<a href="/admin"><i class="fa fa-fw fa-file"></i> Špeciality dňa</a>
			</li>
			<li>
				<a href="/admin/newsletter"><i class="fa fa-fw fa-send"></i> Newsletter</a>
			</li>
			<li>
				<a href="/admin/events"><i class="fa fa-fw fa-star"></i> Akcie</a>
			</li>
		</ul>
	</div>
	<!-- /.navbar-collapse -->
	<?php endif; ?>
</nav>
