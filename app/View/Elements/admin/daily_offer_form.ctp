<?php
echo $this->Form->create($form_name, array(
	'class' => 'daily-menu-form'
));

echo $this->Form->input('DailyOffer.id', array(
	'type' => 'hidden',
	'default' => $daily_offer_id,
	'id' => ''
));

echo $this->Form->input('DailyOffer.appetizer', array(
	'label' => __('Predjedlo'),
	'default' => $data['DailyOffer']['appetizer'],
	'class' => 'form-control',
	'id' => ''
));

echo $this->Form->input('DailyOffer.soup', array(
	'label' => __('Polievka'),
	'default' => $data['DailyOffer']['soup'],
	'class' => 'form-control',
	'id' => ''
));

echo $this->Form->input('DailyOffer.dish', array(
	'label' => __('Hlavné jedlo'),
	'default' => $data['DailyOffer']['dish'],
	'class' => 'form-control',
	'id' => ''
));

echo $this->Form->input('DailyOffer.limonade', array(
	'label' => __('Limonáda'),
	'default' => $data['DailyOffer']['limonade'],
	'class' => 'form-control',
	'id' => ''
));

echo $this->Form->input('DailyOffer.dessert', array(
	'label' => __('Dezert'),
	'default' => $data['DailyOffer']['dessert'],
	'class' => 'form-control',
	'id' => ''
));

echo $this->Form->submit('Uložiť', array(
	'class' => 'btn btn-primary'
));

echo $this->Form->end();
?>