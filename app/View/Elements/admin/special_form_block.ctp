<div class="row specials-row">
	<?php

		echo $this->Form->input('Specials.id', array(
			'type' => 'hidden',
			'default' => $id,
			'id' => false,
			'data-key' => 'id'
		));

		echo $this->Form->input('Specials.text', array(
			'label' => 'Text',
			'default' => $text,
			'class' => 'form-control',
			'id' => false,
			'data-key' => 'text',
			'div' => array('class' => 'input col-xs-12')
		));

		echo $this->Form->input('Specials.weight1', array(
			'label' => 'Hmotnosť 1',
			'default' => $weight1,
			'class' => 'form-control',
			'id' => false,
			'data-key' => 'weight1',
			'div' => array('class' => 'input col-xs-6')
		));

		echo $this->Form->input('Specials.price1', array(
			'label' => 'Cena 1',
			'default' => $price1,
			'class' => 'form-control',
			'id' => false,
			'data-key' => 'price1',
			'div' => array('class' => 'input col-xs-6')
		));

		echo $this->Form->input('Specials.weight2', array(
			'label' => 'Hmotnosť 2',
			'default' => $weight2,
			'class' => 'form-control',
			'id' => false,
			'data-key' => 'weight2',
			'div' => array('class' => 'input col-xs-6')
		));

		echo $this->Form->input('Specials.price2', array(
			'label' => 'Cena 2',
			'default' => $price2,
			'class' => 'form-control',
			'id' => false,
			'data-key' => 'price2',
			'div' => array('class' => 'input col-xs-6')
		));
	?>
	<div class="col-xs-12">
		<hr>
	</div>
</div>
