<div class="col-xs-12 col-sm-4">
	<div class="event-holder" id="event-<?php echo $id; ?>">
		<div>
			<h3><?php echo ($locale == 'en') ?  $title_en : $title; ?></h3>
			<p><?php echo ($locale == 'en') ? $description_en : $description; ?></p>
			<span><?php echo ($locale == 'en') ? $big_text_en : $big_text; ?></span>
		</div>
	</div>
</div>
