<style>

<?php foreach ($full as $photo): ?>
#gallery-item-<?php echo rtrim($photo, '.jpg'); ?> {background-image: url(img/gallery/full/<?php echo $photo; ?>);}
<?php endforeach; ?>

@media (min-width: 1440px) {
<?php foreach ($retina as $photo): ?>
#gallery-item-<?php echo rtrim($photo, '.jpg'); ?> {background-image: url(img/gallery/retina/<?php echo $photo; ?>);}
<?php endforeach; ?>
}

@media (max-width: 767px) {
<?php foreach ($mobile as $photo): ?>
#gallery-item-<?php echo rtrim($photo, '.jpg'); ?> {background-image: url(img/gallery/mobile/<?php echo $photo; ?>);}
<?php endforeach; ?>
}
</style>