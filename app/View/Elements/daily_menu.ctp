<?php if(!empty($appetizer)): ?>
<div class="meal-section">
	<h3>- <?php echo __('Predjedlo'); ?> -</h3>
	<?php foreach (explode("\n", $appetizer) as $value): ?>
	<p class="meal"><?php echo $value; ?></p>
	<?php endforeach; ?>
</div>
<?php endif; ?>

<?php if(!empty($soup)): ?>
<div class="meal-section">
	<h3>- <?php echo __('Polievka'); ?> -</h3>
	<?php foreach (explode("\n", $soup) as $value): ?>
	<p class="meal"><?php echo $value; ?></p>
	<?php endforeach; ?>
</div>
<?php endif; ?>

<?php if(!empty($dish)): ?>
<div class="meal-section">
	<h3>- <?php echo __('Denná ponuka'); ?> -</h3>
	<?php foreach (explode("\n", $dish) as $value): ?>
	<p class="meal"><?php echo $value; ?></p>
	<?php endforeach; ?>
</div>
<?php endif; ?>

<?php if(!empty($dessert)): ?>
<div class="meal-section">
	<h3>- <?php echo __('Dezert dňa'); ?> -</h3>
	<?php foreach (explode("\n", $dessert) as $value): ?>
	<p class="meal"><?php echo $value; ?></p>
	<?php endforeach; ?>
</div>
<?php endif; ?>

<?php if(!empty($limonade)): ?>
<div class="meal-section">
	<h3>- <?php echo __('Limonáda dňa'); ?> -</h3>
	<?php foreach (explode("\n", $limonade) as $value): ?>
	<p class="meal"><?php echo $value; ?></p>
	<?php endforeach; ?>
</div>
<?php endif; ?>
