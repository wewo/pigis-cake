<style>
@media only screen and (-webkit-min-device-pixel-ratio: 1),
only screen and (min--moz-device-pixel-ratio: 1),
only screen and (-moz-min-device-pixel-ratio: 1),
only screen and (-o-min-device-pixel-ratio: 1/1),
only screen and (min-device-pixel-ratio: 1) {
<?php foreach($events_data as $event_data): ?>
#event-<?php echo $event_data['Event']['id']; ?> {
	background-image: url('<?php echo $event_data["Event"]["background_image"]; ?>');
}
<?php endforeach; ?>
}

@media only screen and (-webkit-min-device-pixel-ratio: 2),
only screen and (min--moz-device-pixel-ratio: 2),
only screen and (-moz-min-device-pixel-ratio: 2),
only screen and (-o-min-device-pixel-ratio: 2/1),
only screen and (min-device-pixel-ratio: 2),
only screen and (min-resolution: 192dpi),
only screen and (min-resolution: 2dppx) {
<?php foreach($events_data as $event_data): ?>
#event-<?php echo $event_data['Event']['id']; ?> {
	background-image: url('<?php echo $event_data["Event"]["background_image_retina"]; ?>');+
}
<?php endforeach; ?>
}
</style>
