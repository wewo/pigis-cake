<body style="margin: 0; padding: 0; font-size:14px; font-family: Arial, Helvetica, sans-serif;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="800" style="text-align:center;background-image:url('http://pigis.sk/img/bg/white-noise.jpg');">
					<tr>
						<td style="padding:20px 0px 30px 0px">Ak sa vám e-mail nezobrazuje správne, <a target="_blank" style="color:#60c250;text-decoration:underline" href="http://pigis.sk/preview-email">kliknite sem</a>.</td>
					</tr>
					<tr>
						<td style="padding-bottom:25px"><img width="132" height="136" src="http://pigis.sk/img/logos/pigis-logo-colored.png" alt="Pigis logo"></td>
					</tr>
					<tr>
						<td style="font-size:15px">
							<h2 style="font-size:25px;text-transform:uppercase;margin-top:0px;margin-bottom:15px">Špeciality dňa</h2>
							<p style="margin: 0px auto 10px auto"><span id="newsletter-date"><?php echo $newsletter_data['date']; ?></span>&nbsp;<span id="newsletter-opening-time"><?php echo $newsletter_data['opening_time']; ?></span></p>
						</td>
					</tr>

					<?php if(!empty($daily_offer_data['appetizer'])): ?>
					<tr>

						<td style="font-size:15px">
							<h3 style="text-transform:uppercase">– Predjedlo –</h3>
							<?php foreach (explode("\n", $daily_offer_data['appetizer']) as $value): ?>
							<p style="margin: 0px auto 10px auto"><?php echo $value; ?></p>
							<?php endforeach; ?>
						</td>
					</tr>
					<?php endif; ?>

					<?php if(!empty($daily_offer_data['soup'])): ?>
					<tr>
						<td style="font-size:15px">
							<h3 style="text-transform:uppercase">– Polievka –</h3>
							<?php foreach (explode("\n", $daily_offer_data['soup']) as $value): ?>
							<p style="margin: 0px auto 10px auto"><?php echo $value; ?></p>
							<?php endforeach; ?>
						</td>
					</tr>
					<?php endif; ?>

					<?php if(!empty($daily_offer_data['dish'])): ?>
					<tr>
						<td style="font-size:15px">
							<h3 style="text-transform:uppercase">– Denná ponuka –</h3>
							<?php foreach (explode("\n", $daily_offer_data['dish']) as $value): ?>
							<p style="margin: 0px auto 10px auto"><?php echo $value; ?></p>
							<?php endforeach; ?>
						</td>
					</tr>
					<?php endif; ?>

					<?php if(!empty($daily_offer_data['limonade'])): ?>
					<tr>
						<td style="font-size:15px">
							<h3 style="text-transform:uppercase">– Limonáda dňa –</h3>
							<?php foreach (explode("\n", $daily_offer_data['limonade']) as $value): ?>
							<p style="margin: 0px auto 10px auto"><?php echo $value; ?></p>
							<?php endforeach; ?>
						</td>
					</tr>
					<?php endif; ?>

					<?php if(!empty($daily_offer_data['dessert'])): ?>
					<tr>
						<td style="font-size:15px">
							<h3 style="text-transform:uppercase">– Dezert dňa –</h3>
							<?php foreach (explode("\n", $daily_offer_data['dessert']) as $value): ?>
							<p style="margin: 0px auto 10px auto"><?php echo $value; ?></p>
							<?php endforeach; ?>
						</td>
					</tr>
					<?php endif; ?>


					<tr>
						<td>
							<p style="font-weight:bold;">V pracovné dni od 14.30 do 15.30 a v nedeľu nevaríme.</p>
							<p>Viac informácií nájdete na stránke <a style="color:#60c250;text-decoration:underline" target="_blank" href="http://www.pigis.sk/">www.pigis.sk</a></p>
						</td>
					</tr>
					<?php if( !empty($newsletter_data['image_url'])): ?>
					<tr>
						<td style="padding-top:40px; padding-bottom:50px">
							<img id="newsletter-image" src="<?php echo $newsletter_data['image_url']; ?>" alt="<?php echo $newsletter_data['image_alt']; ?>" width="600" height="300">
						</td>
					</tr>
					<?php endif; ?>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="800" style="color:white;background-image:url('http://pigis.sk/img/bg/black-noise.jpg');font-size:15px">

					<tr>
						<td colspan="3" style="padding-top:50px; padding-bottom:30px;text-align:center">
							<img width="104" height="52" src="http://pigis.sk/img/logos/pigis-white.png" alt="Pigis logo">
						</td>
					</tr>

					<tr>
						<td width="360">
							<table width="260" style="margin-left:50px">
								<tr>
									<td>
										<h3 style="text-transform:uppercase;font-size:15px;font-weight:bold">Adresa</h3>
									</td>
								</tr>
								<tr>
									<td style="color:white">Dolná 50<br>974 01  Banská Bystrica</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td>
										<h3 style="text-transform:uppercase;font-size:15px;font-weight:bold">Rezervácie</h3>
									</td>
								</tr>
								<tr>
									<td style="color:white">0949 167 367, <a style="color:#60c250;text-decoration:underline" href="mailto:pigis@pigis.sk">pigis@pigis.sk</a></td>
								</tr>
							</table>
						</td>
						<td width="300" valign="top">
							<table>
								<tr>
									<td>
										<h3 style="text-transform:uppercase;font-size:15px;font-weight:bold;color:white">Otváracie hodiny</h3>
									</td>
								</tr>
								<tr>
									<td>
										<table>
											<tr>
												<td width="80" style="color:white">Po-Št</td>
												<td style="color:white">8.0 - 22.00</td>
											</tr>
											<tr>
												<td style="color:white" width="80">Piatok</td>
												<td style="color:white">8.00 - 23.00</td>
											</tr>
											<tr>
												<td style="color:white" width="80">Sobota</td>
												<td style="color:white">11.00 - 23.00</td>
											</tr>
											<tr>
												<td style="color:white" width="80">Nedeľa</td>
												<td style="color:white">11.00 - 20.00</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<p style="color:#999999"><br>Cez pracovné dni čase<br>od 14.30 do 15.30<br>a v nedeľu je kuchyňa zatvorená.</p>
									</td>
								</tr>
							</table>
						</td>
						<td>
							<table>
								<tr>
									<td style="padding-bottom: 10px">
										<a style="display:block" target="_blank" href="https://www.facebook.com/Pigis">
											<img src="http://pigis.sk/img/icons/fb.png" alt="">
										</a>
									</td>
								</tr>
								<tr>
									<td style="padding-bottom: 10px">
										<a style="display:block" target="_blank" href="https://www.instagram.com/pigis_bistro/">
											<img src="http://pigis.sk/img/icons/instagram.png" alt="">
										</a>
									</td>
								</tr>
								<tr>
									<td style="padding-bottom: 10px">
										<a style="display:block" target="_blank" href="https://foursquare.com/v/pigis/4f3cb6bde4b00758870d0332">
											<img src="http://pigis.sk/img/icons/f.png" alt="">
										</a>
									</td>
								</tr>
								<tr>
									<td style="padding-bottom: 10px">
										<a style="display:block" target="_blank" href="https://www.tripadvisor.sk/Restaurant_Review-g274923-d4505799-Reviews-Pigis-Banska_Bystrica_Banska_Bystrica_Region.html">
											<img src="http://pigis.sk/img/icons/trip-advisor.png" alt="">
										</a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="3" style="padding-top:20px; padding-bottom:20px">
							<p style="font-size:13px;text-align:center; color:#666666">Ak si neželáte dostávať tieto e-maily, tu sa môžete <a style="color:#666666;text-decoration:underline" href="http://pigis.sk/unsubscribe/<?php echo $random_string; ?>">odhlásiť</a>.</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
