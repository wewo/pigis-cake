<?php foreach ($full as $photo): ?>
	<?php $photo_name = str_replace(".jpg", "", $photo); ?>
	<div class="item">
		<div id="gallery-item-<?php echo $photo_name; ?>" alt="Gallery item #<?php echo $photo_name; ?>"></div>
	</div>
<?php endforeach; ?>
