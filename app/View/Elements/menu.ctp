<?php
if($locale == 'sk') {
	$changeLangLinkText = array('short' => 'EN', 'long' => 'EN');
	$changeLangLinkUrl = '/en';
}
else {
	$changeLangLinkText = array('short' => 'SK', 'long' => 'SK');
	$changeLangLinkUrl = '/sk';
}
?>

<nav class="navbar navbar-absolute-top">
	<div class="container">
		<div class="row">
			<div class="col-lg-offset-1 col-lg-11">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"></a>
				</div>

				<div class="collapse navbar-collapse" id="navbar-collapse">
					<ul class="nav navbar-nav">
						<li><?php echo __(''); ?>
							<a href="#uvod" data-scroll-to="#uvod">
								<span><?php echo __('Úvod'); ?></span>
							</a>
						</li>
						<li>
							<a href="#denne-menu" data-scroll-to="#burger">
								<span><?php echo __('Špeciality dňa'); ?></span>
							</a>
						</li>
						<?php if(0): ?>
						<li>
							<a href="#speciality" data-scroll-to="#burger">
								<span><?php echo __('Špeciality'); ?></span>
							</a>
						</li>
						<?php endif; ?>
						<li>
							<a href="#akcie" data-scroll-to="#events">
								<span><?php echo __('Akcie'); ?></span>
							</a>
						</li>
						<li>
							<a href="#galeria" data-scroll-to="#gallery">
								<span><?php echo __('Fotogaléria'); ?></span>
							</a>
						</li>
						<li>
							<a href="#makronkovo" data-scroll-to="#makronkovo">
								<span><?php echo __('Makrónkovo'); ?></span>
							</a>
						</li>
						<li>
							<a href="#kontakt" data-scroll-to="footer">
								<span><?php echo __('Kontakt'); ?></span>
							</a>
						</li>
						<li id="language-switch-link-fullscreen">
							<a href="<?php echo $changeLangLinkUrl; ?>">
								<span style="margin-right:15px; color:#61c250">/</span><span><?php echo $changeLangLinkText['short']; ?></span>
							</a>
						</li>
						<li id="language-switch-link-mobile">
							<a href="<?php echo $changeLangLinkUrl; ?>">
								<span><?php echo $changeLangLinkText['long']; ?></span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</nav>
