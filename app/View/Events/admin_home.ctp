<div class="row">
	<div class="col-xs-12 col-md-6">
	<?php
		echo $this->Form->create('Event', array(
			'class' => 'events-form'
		));

		foreach ($events_data as $i => $event_data) {
			echo '<div class="event-row">';

			echo		$this->Form->input('Event.id', array(
							'label' => false,
							'default' => $event_data['Event']['id'],
							'data-key' => 'id',
							'type' => 'hidden'
			));

			echo '<div class="row">';
			echo	'<div class="col-xs-12 col-md-6">';
			echo		$this->Form->input('Event.title', array(
							'label' => 'SK Názov',
							'class' => 'form-control',
							'default' => $event_data['Event']['title'],
							'data-key' => 'title'
			));
			echo	'</div>';
			echo	'<div class="col-xs-12 col-md-6">';
			echo		$this->Form->input('Event.title_en', array(
							'label' => 'EN Názov',
							'class' => 'form-control',
							'default' => $event_data['Event']['title_en'],
							'data-key' => 'title_en'
						));
			echo	'</div>';
			echo '</div>';

			echo '<div class="row">';
			echo	'<div class="col-xs-12 col-md-6">';
			echo		$this->Form->input('Event.description', array(
							'label' => 'SK Popis',
							'class' => 'form-control',
							'default' => $event_data['Event']['description'],
							'data-key' => 'description'
						));
			echo	'</div>';
			echo	'<div class="col-xs-12 col-md-6">';
			echo		$this->Form->input('Event.description_en', array(
							'label' => 'EN Popis',
							'class' => 'form-control',
							'default' => $event_data['Event']['description_en'],
							'data-key' => 'description_en'
						));
			echo	'</div>';
			echo '</div>';

			echo '<div class="row">';

			echo	'<div class="col-xs-12 col-md-6">';
			echo		$this->Form->input('Event.big_text', array(
							'label' => 'SK Big text',
							'class' => 'form-control',
							'default' => $event_data['Event']['big_text'],
							'data-key' => 'big_text'
						));
			echo	'</div>';
			echo	'<div class="col-xs-12 col-md-6">';
			echo		$this->Form->input('Event.big_text_en', array(
							'label' => 'EN Big text',
							'class' => 'form-control',
							'default' => $event_data['Event']['big_text_en'],
							'data-key' => 'big_text_en'
						));
			echo	'</div>';
			echo '</div>';

			echo '<div class="row">';

			echo	'<div class="col-xs-12">';
			echo		$this->Form->input('Event.background_image', array(
							'label' => 'Background image URL',
							'class' => 'form-control',
							'default' => $event_data['Event']['background_image'],
							'data-key' => 'background_image'
						));
			echo	'</div>';
			echo	'<div class="col-xs-12">';
			echo		$this->Form->input('Event.background_image_retina', array(
							'label' => 'Background image URL - retina',
							'class' => 'form-control',
							'default' => $event_data['Event']['background_image_retina'],
							'data-key' => 'background_image_retina'
						));
			echo	'</div>';
			echo '</div>';
			echo '</div>';

			if( $i+1 != sizeof($events_data) ) {
				echo '<hr><hr><hr>';
			}
		}

		echo $this->Form->submit('Uložiť', array(
			'class' => 'btn btn-primary',
			'div' => array('style' => 'margin-bottom:15px')
		));

		echo '<p class="success">Dáta boli uložené</p>';

		echo $this->Form->end();
	?>
	</div>
</div>
