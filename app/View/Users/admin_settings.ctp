<div class="row">
	<div class="col-xs-12 col-md-2">
	<?php
	echo $this->Form->create('User', array('action' => 'settings'));

	echo $this->Form->input('old_pass', array(
		'type' => 'password',
		'div' => array('class'=>'form-group'),
		'label' => __('Aktuálne heslo'),
		'class' => 'form-control',
	));

	echo $this->Form->input('pass', array(
		'type' => 'password',
		'label' => __('Nové heslo'),
		'div' => array('class'=>'form-group'),
		'class' => 'form-control',
		'error' => array(
			'between' => __('Heslo nesmie mať menej ako 8 znakov a viac ako 20 znakov.'),
			'compare' => __('Heslo a overenie hesla sa musia zhodovať.')
		)
	));

	echo $this->Form->input('pass2', array(
		'type' => 'password',
		'div' => array('class'=>'form-group'),
		'label' => __('Overenie hesla'),
		'class' => 'form-control',
	));

	echo $this->Form->submit(__('Uložiť zmeny'), array('class' => 'btn btn-primary'));
	echo $this->Form->end(); 

	?>
	</div>
</div>
