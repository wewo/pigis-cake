<div class="row">
	<div class="col-xs-12 col-md-4">
		<?php
		echo $this->Form->create('User', array('action' => 'login'));
		echo $this->Form->input('name', array(
			'label' => false, 
			'div' => false,
			'placeholder' => __('User'),
			'class' => 'form-control',
			'div' => 'form-group'
		));

		echo $this->Form->input('password', array(
			'label' => false,
			'div' => false,
			'placeholder' => __('Heslo'),
			'class' => 'form-control',
			'div' => 'form-group'
		));

		echo $this->Form->submit(__('Prihlásiť sa'), array('class' => 'btn btn-default')); 

		echo $this->Form->end(); 
		?>
	</div>
</div>