<?php
App::uses('AppModel', 'Model');

class User extends AppModel {

	public $hasMany = array('PasswordRecord');
	public $validate = array(
		'pass' => array (
			'strongPass'	=> array(
				'rule'		=> 'checkPasswordStrength',
				'message'	=> 'Slabé heslo'
			),
			'between'	=> array(
				'rule'	=> array('between', 8, 20)
			),
			'compare'	=> array(
				'rule'	=> array('compare_passoword', 'pass2')
			)
		),
		'username' => array(
			'required'		=> array(
				'rule'		=> array('notEmpty'),
				'message'	=> 'A username is required'
			)
		),
		'password' => array(
			'required'		=> array(
				'rule'		=> 'checkPasswordStrength',
				'message'	=> 'Slabé heslo'
			)
		)		
	);

	public function compare_passoword($pass1 = null, $pass2 = null) {

		foreach ($pass1 as $key => $value) {
			if ($value != $this->data[$this->name][$pass2]) {
				return false;
			}
			else continue;
		}
		return true;
	}

	public function checkPasswordStrength( $password ) {

		$pass_arr_keys = array_keys($password);
		$password = $password[ $pass_arr_keys[0] ];

		$r1='/[A-Z]/';  //Uppercase
		$r2='/[a-z]/';  //lowercase
		$r3='/[!@#$%^&*()\-_=+{};:,<.>]/';  // whatever you mean by 'special char'
		$r4='/[0-9]/';  //numbers

		if( !preg_match_all( $r1, $password, $out ) ) return false;
		if( !preg_match_all( $r2, $password, $out ) ) return false;
		if( !preg_match_all( $r3, $password, $out ) ) return false;
		if( !preg_match_all( $r4, $password, $out ) ) return false;

		return true;
	}
}
?>