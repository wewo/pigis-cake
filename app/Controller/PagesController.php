<?php

App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class PagesController extends AppController {

	public $name = 'Pages';
	public $uses = array('Event', 'DailyOffer', 'Special', 'ConfigValue', 'Newsletter', 'NewsletterAddress');
	public $helpers = array('Time');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('hideDailyOffer', 'home', 'changeLanguage', 'testEmail', 'previewEmail');
	}

	public function home() {
		
		$pageSubtitle = __('');

		$this->set( 'pageTitle', __('Pigis - lokálne bistro') );
		$this->set( 'pageSubtitle', $pageSubtitle);
		$this->set( 'daily_menu_data', $this->getDailyOfferData());
		$this->set( 'specials_data', $this->getSpecialsData(Configure::read('Config.language')));
		// $this->set( 'config_values', $this->getConfigValues(Configure::read('Config.language')));
		$this->set( 'config_values', $this->getConfigValues('sk'));
		$this->set( 'gallery_image_file_names', $this->getGalleryImageFileNames());
		$this->set( 'events_data', $this->Event->find('all') );
	}

	private function getDailyOfferData($id = null) {

		if(empty($id)) {

			if( Configure::read('Config.language') == 'en' ) {
				$id = EN_DAILY_OFFER_ID;
			}
			else {
				$id = SK_DAILY_OFFER_ID;
			}
		}

		// showing only SK version since 04. 09. 2016
		$id = SK_DAILY_OFFER_ID;

		$daily_menu = $this->DailyOffer->find('first', array(
			'conditions' => array('id' => $id)
		));

		return $daily_menu;
	}

	private function getSpecialsData($lang = null) {

		$conditions = array();

		if( !empty($lang) ) {
			$conditions['lang'] = $lang;
		}

		$specials_data = $this->Special->find('all', array(
			'conditions' => $conditions
		));

		return $specials_data;
	}

	private function getConfigValues($lang = null) {

		$conditions = array();

		if( !empty($lang) ) {
			$conditions['lang'] = $lang;
		}

		$specials_data = $this->ConfigValue->find('all', array(
			'conditions' => $conditions
		));

		return $specials_data;
	}

	public function admin_home() {
		$this->set( 'pageTitle', __('Admin') );
		$this->set( 'pageSubtitle', 'Špeciality dňa');

		$this->set('sk_daily_menu_data', $this->getDailyOfferData(SK_DAILY_OFFER_ID));
		$this->set('en_daily_menu_data', $this->getDailyOfferData(EN_DAILY_OFFER_ID));
		$this->set('specials_data', $this->getSpecialsData());
		$this->set('config_values', $this->getConfigValues());
	}

	public function updateDailyOffer() {
		if( !$this->request->is('post') || !$this->request->is('ajax') || empty($this->request->data) ) {
			exit;
		}

		$this->DailyOffer->id = $this->request->data['id'];
		$this->DailyOffer->set($this->request->data);
		$this->DailyOffer->save();

		$this->ConfigValue->id = 3;
		$this->ConfigValue->saveField('value', 'true');

		$this->ConfigValue->id = 4;
		$this->ConfigValue->saveField('value', 'true');

		$this->autoRender = false;
	}

	public function updateSpecials() {
		if( !$this->request->is('post') || !$this->request->is('ajax') || empty($this->request->data) ) {
			exit;
		}

		$data = array();

		foreach ($this->request->data as $key => $special) {
			if($key !== 'specials_name' && $key !== 'config-val-id') {
				$data[]['Special'] = $special;
			}
		}

		// debug( $data );
		debug( $this->Special->saveMany($data) );

		$this->ConfigValue->id = $this->request->data['config-val-id'];
		$this->ConfigValue->saveField('value', $this->request->data['specials_name']);

		$this->autoRender = false;
	}

	private function getGalleryImageFileNames() {
		$fullGalleryDir = new Folder('img/gallery/full');
		$retinaGalleryDir = new Folder('img/gallery/retina');
		$mobileGalleryDir = new Folder('img/gallery/mobile');

		return array(
			'full' => $fullGalleryDir->find('.*\.jpg'),
			'retina' => $retinaGalleryDir->find('.*\.jpg'),
			'mobile' => $mobileGalleryDir->find('.*\.jpg')
		);
	}

	public function previewEmail() {
		$this->set( 'random_string', '');
		$this->set( 'newsletter_data', $this->getNewsletterData());
		$this->set( 'daily_offer_data', $this->getDailyOfferData(SK_DAILY_OFFER_ID)['DailyOffer']);

		$this->layout = 'Emails/html/default';
		return $this->render('/Emails/html/daily_offer');
	}

	public function testEmail($email = null) {

		if( empty($email) ) {
			exit;
		}

		$email = $this->initEmail(
			$email,
			'Pigis denné menu',
			'daily_offer',
			array(
				'random_string' => '',
				'newsletter_data' => $this->getNewsletterData(),
				'daily_offer_data' => $this->getDailyOfferData(SK_DAILY_OFFER_ID)
			)
		);
		$email->send();

		$this->autoRender = false;
	}

	public function admin_newsletter() {
		$this->set( 'pageTitle', __('Pigis | Admin') );
		$this->set( 'pageSubtitle', 'Newsletter');
		$this->set( 'newsletter_data', $this->getNewsletterData());
		$this->set( 'random_string', '');
		$this->set( 'daily_offer_data', $this->getDailyOfferData(SK_DAILY_OFFER_ID)['DailyOffer']);
		$this->set( 'newsletter_addresses_list', $this->getNewsletterAddresses('list') );
	}

	private function getNewsletterData() {
		$newsletter_data = $this->Newsletter->find('first', array(
			'conditions' => array('id'=>1)
		));

		return $newsletter_data['Newsletter'];
	}

	public function updateNewsletter() {
		if( !$this->request->is('post') || empty($this->request->data) ) {
			exit;
		}

		$this->Newsletter->id = 1;
		$this->Newsletter->set($this->request->data);
		$this->Newsletter->save();

		$this->autoRender = false;
	}

	private function getNewsletterAddresses($mode = 'list') {

		$newsletter_addresses = array();
		if( $mode == 'list' ) {
			$newsletter_addresses = $this->NewsletterAddress->find('list', array(
				'conditions' => array('enabled' => 1),
				'fields' => array('email')
			));
		}
		else if( $mode == 'full' ) {
			$newsletter_addresses = $this->NewsletterAddress->find('all', array(
				'conditions' => array('enabled' => 1),
				'fields' => array('id', 'email', 'random_string')
			));
		}

		return $newsletter_addresses;
	}

	public function sendNewsletter() {

		if( !$this->request->is('post') || !$this->request->is('ajax') ) {
			exit;
		}

		$newsletter_addresses = $this->getNewsletterAddresses('full');

		foreach ($newsletter_addresses as $address) {
			$email = $this->initEmail(
				$address['NewsletterAddress']['email'],
				'Pigis - špeciality dňa',
				'daily_offer',
				array(
					'random_string' => $address['NewsletterAddress']['random_string'],
					'newsletter_data' => $this->getNewsletterData(),
					'daily_offer_data' => $this->getDailyOfferData(SK_DAILY_OFFER_ID)['DailyOffer']
				)
			);
			$email->send();
		}

		$this->autoRender = false;
	}

	public function hideDailyOffer() {
		$this->ConfigValue->id = 3;
		$this->ConfigValue->saveField('value', 'false');

		$this->ConfigValue->id = 4;
		$this->ConfigValue->saveField('value', 'false');
		$this->autoRender = false;
	}
}
