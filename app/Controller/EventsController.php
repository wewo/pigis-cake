<?php

App::uses('AppController', 'Controller');

class EventsController extends AppController {

	public $name = 'Events';
	public $uses = array();

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('singUp');
	}

	private function getEventsData() {
		return $this->Event->find('all');
	}

	public function update() {

		if( !$this->request->is('post') || !$this->request->is('ajax') || empty($this->request->data) ) {
			exit;
		}
		
		$this->Event->saveAll($this->request->data);
		$this->autoRender = false;
	}

	public function admin_home() {
		$this->set( 'pageTitle', __('Pigis | Admin') );
		$this->set( 'pageSubtitle', 'Akcie');

		$this->set( 'events_data', $this->getEventsData() );
	}
}
