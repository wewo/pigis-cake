<?php
App::uses('FormAuthenticate', 'Controller/Component/Auth');

class CustomAuthenticate extends FormAuthenticate {

	public $uses = array('User');
	/**
	 * Find a user record using the standard options.
	 *
	 * The $username parameter can be a (string)username or an array containing
	 * conditions for Model::find('first'). If the $password param is not provided
	 * the password field will be present in returned array.
	 *
	 * Input passwords will be hashed even when a user doesn't exist. This
	 * helps mitigate timing attacks that are attempting to find valid usernames.
	 *
	 * @param string|array $username The username/identifier, or an array of find conditions.
	 * @param string $password The password, only used if $username param is string.
	 * @return bool|array Either false on failure, or an array of user data.
	 */
	protected function _findUser($username, $password = null) {
		$userModel = $this->settings['userModel'];
		list(, $model) = pluginSplit($userModel);
		$fields = $this->settings['fields'];

		if (is_array($username)) {
			$conditions = $username;
		} else {
			$conditions = array(
				$model . '.' . $fields['username'] => $username
			);
		}

		if (!empty($this->settings['scope'])) {
			$conditions = array_merge($conditions, $this->settings['scope']);
		}

		$result = ClassRegistry::init($userModel)->find('first', array(
			'conditions' => $conditions,
			'recursive' => $this->settings['recursive'],
			'contain' => $this->settings['contain'],
		));

		if ( empty($result) || empty($result[$model]) || $result[$model][$fields['password']] != $this->_password($password, $result[$model]['id'], $result[$model]['salt'] ) ) {
			$this->passwordHasher()->hash($password);
			return false;
		}

		$user = $result[$model];
		if ($password !== null) {
			if (!$this->passwordHasher()->check($password, $user[$fields['password']])) {
				return false;
			}
			unset($user[$fields['password']]);
		}

		unset($result[$model]);

		return array_merge($user, $result);
	}


	/**
	 * Hash the plain text password so that it matches the hashed/encrypted password
	 * in the datasource.
	 *
	 * @param string $password The plain text password.
	 * @return string The hashed form of the password.
	 * @deprecated Since 2.4. Use a PasswordHasher class instead.
	 */
	protected function _password($password, $id=null, $salt=null) {

		// $salt = "$2y$10$".bin2hex(openssl_random_pseudo_bytes(22));

		Security::setHash('blowfish');
		return Security::hash($password, 'blowfish', $salt);
	}
}

?>