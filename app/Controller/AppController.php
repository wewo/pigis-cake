<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $components = array(
		'Auth' => array(
			'loginAction'		=> array('controller' => 'users', 'action' => 'login', 'admin'=>true),
			'loginRedirect'		=> array('controller' => 'pages', 'action' => 'home'),
			'logoutRedirect'	=> array('controller' => 'pages', 'action' => 'home'),
			'authError'			=> 'Did you really think you are allowed to see that?',
			'authenticate'	=> array(
				'Custom'	=> array(
					'fields'			=> array('username' => 'name'),
					'passwordHasher'	=> 'Blowfish'
				)
			)
		),
		'DebugKit.Toolbar', 'RequestHandler',
		'Session'

	);

	public function beforeFilter() {
		if ($this->Auth->loggedIn()) {
			$this->logged = $this->Auth->user();
		}

		if ($this->Session->check('Config.language')) {
			Configure::write('Config.language', $this->Session->read('Config.language'));
		}
		else {
			Configure::write('Config.language', 'sk');
		}
		$this->set('locale', Configure::read('Config.language'));

		//admin stuff
		if (!empty($this->request->params['admin'])) {
			$this->layout = 'admin_default';
			setlocale(LC_TIME, 'sk_SK.UTF8');
			$this->set('locale', Configure::read('Config.language'));
			Configure::write('Config.language', 'sk');
		}

		$this->set('logged', $this->logged);
	}

	protected function initEmail($to, $subject, $template, $data = array(), $layout = 'default', $from = null, $type = 'html', $attachements = null) {

		App::uses('CakeEmail', 'Network/Email');
		
		$email = new CakeEmail('default');

		$from = array('pigis@pigis.sk' => 'Pigis');

		$email->from($from);
		$email->to($to);
		$email->subject($subject);
		$email->emailFormat($type);
		$email->template($template, $layout);

		if (!empty($attachements)) {
			$email->attachments($attachements);
		}

		if (!empty($data)) {
			$email->viewVars($data);
		}

		return $email;
	}

	protected function initFilter() {
		unset($this->request->query['ext']);
		unset($this->request->query['x']);
		unset($this->request->query['y']);

		if (!empty($this->request->query)) {
			$this->request->data['Filter'] = $this->request->query;
		}
		elseif (!empty($this->passedArgs)) {
			$this->request->data['Filter'] = $this->passedArgs;
		}
	}

	public function changeLanguage($lang) {
		if(!empty($lang)) {
			if($lang == 'sk'){
				$this->Session->write('Config.language', 'sk');
			}

			else if($lang == 'en'){
				$this->Session->write('Config.language', 'en');
			}

			$this->redirect($this->referer());
		}
	}
}
