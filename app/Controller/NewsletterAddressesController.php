<?php

App::uses('AppController', 'Controller');

class NewsletterAddressesController extends AppController {

	public $name = 'NewsletterAddresses';
	public $uses = array();

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('singUp', 'unsubscribe');
	}

	public function singUp() {

		if( !$this->request->is('post') || empty($this->request->data['email']) ) {
			exit;
		}

		$this->autoRender = false;

		$email = $this->NewsletterAddress->find('first', array(
			'conditions' => array('email' => $this->request->data['email']),
			'fields' => array('id', 'email')
		));

		if( !empty($email) ) {
			return json_encode( array('result'=>'0', 'message'=>'exists') );
		}

		$data = array(
			'random_string' => $this->generateRandomString(),
			'email' => $this->request->data['email']
		);

		$this->NewsletterAddress->create();
		$this->NewsletterAddress->set($data);
		if( $this->NewsletterAddress->save() ) {
			return json_encode( array('result'=>'1', 'message'=>'success') );
		}

		return json_encode( array('result'=>'0', 'message'=>'error') );
		
	}

	private function generateRandomString($length = 32) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';

		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function unsubscribe($random_string = null) {
		if( empty($random_string) ) {
			exit;
		}

		$found = $this->NewsletterAddress->find('first', array(
			'conditions' => array(
				'random_string' => $random_string,
				'enabled' => 1
			),
			'fields' => array('id')
		));

		if ( empty($found) ) {
			exit;
		}

		$this->NewsletterAddress->id = $found['NewsletterAddress']['id'];
		$this->NewsletterAddress->saveField('enabled', 0);

		echo '<p style="text-align:center">Email bol odhlásený z odberu newslettru.</p>';

		$this->autoRender = false;
	}
}
