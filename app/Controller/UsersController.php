<?php

App::uses('AppController', 'Controller');

class UsersController extends AppController {
	public $name = 'Users';
	public $uses = array('User');
	public $components = array('Session');

	public function beforeFilter() {
		parent::beforeFilter( );
	}

	public function admin_login() {

		$this->set( 'pageTitle', __('Login') );
		$this->set( 'pageSubtitle', __('Prihláste sa do systému') );

		// $salt = "$2y$10$".bin2hex(openssl_random_pseudo_bytes(22));
		// $salt = '';
		// $password = Security::hash( '', 'blowfish', $salt );
		// debug($password);

		$this->set('title_for_layout', __('Login'));
		// $this->layout = 'login';
	
		if ($this->logged != null) {
			$this->redirect($this->Auth->loginRedirect);
		}
		
		if ($this->request->is('post')) {

			$checkedUserAccess = $this->checkUserAccess( $this->request->data['User']['name'] );

			if ( !empty( $checkedUserAccess ) && $this->Auth->login() ) {
				return $this->redirect($this->Auth->redirectUrl());
			}
			else {
				$this->Session->setFlash(__('Prístup zamietnutý'));
			}
		}
	}

	public function admin_logout() {
		$this->redirect($this->Auth->logout());
	}

	public function admin_settings() {

		$this->set( 'pageTitle', __('Nastavenia') );
		$this->set( 'pageSubtitle', __('Zmena hesla užívateľa') );
		$id = (int) $this->logged['id'];

		//checkneme ci user existuje
		$user = $this->User->find('first', array(
			'conditions' => array(
				'User.id' => $id
			)
		));

		if (empty($user)) {
			throw new NotFoundException();
		}

		if ( !empty($this->request->data) ) {

			//validate pass
			if( !$this->validateUserPwd($user) ) {
				$this->Session->setFlash(__('Nesprávne údaje'), FLASH_ERROR);
			}
			//ak nemame ziadne chyby pri zmene hesla
			else if (empty($this->User->validationErrors)) {
				$this->request->data['User']['id'] = $id;
				unset($this->request->data['User']['pass']);
				$this->User->set($this->request->data);

				if ($this->User->validates()) {
					if ($this->User->save()) {
						$this->Session->setFlash(__('Nastavenia boli úspešne zmenené.'), FLASH_OK);
						$this->redirect(array('controller' => 'users', 'action' => 'settings'));
					}
					else {
						$this->Session->setFlash(__('Počas zmeny nastavní nastala bližšie nešpecifikovaná chyba. Prosím skúste sa ich zmeniť znovu.'), FLASH_ERROR);
					}
				}
				else {
					// debug( $this->User->validationErrors );
					$this->Session->setFlash( __('Chyba pri validacii'), FLASH_ERROR );
				}
			}
			else {
				// debug( $this->User->validationErrors );
				$this->Session->setFlash( __('Chyba pri validacii'), FLASH_ERROR );
			}
		}
		else {
			unset( $user['User']['id'] );
			$this->request->data = $user;
		}
		$this->request->data['User']['old_pass'] = $this->request->data['User']['pass'] = $this->request->data['User']['pass2'] = '';
	}

	/**
	 * Ovaliduje stare heslo, overi nove heslo, ak je vsetko spravne
	 * tak nastavi userovi nove heslo a salt
	 * 
	 * @author matus.bielik
	 */
	private function validateUserPwd($user) {
		if ($this->request->data['User']['old_pass'] != '' && $this->request->data['User']['pass'] != '' && $this->request->data['User']['pass2'] != '') {
			
			// overi ci sa heslo nepouzilo poslednych 13 krat
			if( !$this->checkPasswordRecords( $this->request->data['User']['pass'] ) ) {
				return false;
			}

			//stare heslo musi byt spravne
			if ($user['User']['password'] == Security::hash($this->request->data['User']['old_pass'], 'blowfish', $user['User']['password'])) {
				
				$this->User->set($this->request->data);
				//ovalidujeme nove hesla
				if ( $this->User->validates( array('fieldList' => array('pass')) ) ) {

					$salt = "$2y$10$".bin2hex(openssl_random_pseudo_bytes(22));

					$this->request->data['User']['password'] = Security::hash( $this->request->data['User']['pass'], 'blowfish', $salt );
					$this->request->data['User']['salt'] = $salt;

					$this->newPasswordRecord( $this->request->data['User'] );
					return true;
				}
				else {
					return true;
				}
			}
			else {
				return false;
				// $this->User->invalidate('old_pass', __('Zadali ste nesprávne pôvodné heslo.'));
			}
		} //end zmena hesla
		else {
			return false;
		}
	}

	private function checkPasswordRecords( $pass = null ) {

		$records = $this->User->PasswordRecord->find('all', array(
			'conditions' => array('user_id'=> $this->logged['id'])
		));

		Security::setHash('blowfish');

		foreach ($records as $i => $record) {
			if( $record['PasswordRecord']['password'] == Security::hash($pass, 'blowfish', $record['PasswordRecord']['salt']) ) {
				return false;
			}
		}

		return true;
	}

	private function newPasswordRecord( $user = null ) {

		if( $user == null )
			return false;

		$this->User->PasswordRecord->create();
		$this->User->PasswordRecord->set( array( 'user_id' => $this->logged['id'], 'password' => $user['password'], 'salt' => $user['salt'] ) );
		$this->User->PasswordRecord->save();

		$records = $this->User->PasswordRecord->find('list', array(
			'conditions' => array('user_id'=> $this->logged['id']),
			'fields'=> array('id', 'created'),
			'order' => 'created DESC'
		));

		if( count( $records ) > 12 ) {
			end( $records );

			$lastRecord = key($records);
			$this->User->PasswordRecord->delete( $lastRecord );
		}

		$this->autoRender = false;
	}

	// skontroluje ci je uzivatel zamknuty, a ci uz odvtedy ubehla hodina
	private function checkUserAccess( $username ) {

		// ziskanie zaznamu uzivatela
		$user =  $this->User->find('first', array(
			'conditions' => array('name' => $username)
		));

		// ak sa uzivatel nenasiel
		if( empty( $user ) ) {
			return false;
		}

		// ak je uzivatel prave zamknuty
		if( $user['User']['locked'] == true ) {

			App::uses('CakeTime', 'Utility');
			if( CakeTime::isPast( $user['User']['locked_until'] ) ) {

				return $this->User->save( array(
					'id' => $user['User']['id'],
					'wrong_tries' => 0,
					'locked' => false,
					'locked_until' => null
				), false);

			}
			else {
				return false;
			}

		}

		return true;
	}

	private function wrongLoginTry( $username ) {
		// ziskanie zanamu uzivatela
		$user =  $this->User->find('first', array(
			'conditions' => array('name'=>$username)
		));

		// ak sa uzivatel nenasiel
		if( empty( $user ) ) {
			return false;
		}

		$user['User']['wrong_tries'] += 1;
		if( $user['User']['wrong_tries'] >=10 ) {
			$user['User']['locked'] = true;
			$user['User']['locked_until'] = date('Y-m-d G:i:s', strtotime('+1 hour') );
		}		

		$this->User->save( $user, false );
	}

	private function resetWrongLoginAttempts( $username ) {
		$user =  $this->User->find('first', array(
			'fields'	=> array('id'),
			'conditions' => array('name'=>$username)
		));
		$this->User->save( array(
			'id'	=> $user['User']['id'],
			'wrong_tries' => 0
		), false );
	}
}

?>
