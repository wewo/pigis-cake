# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: Controller/PagesController.php:17
msgid ""
msgstr ""

#: Controller/PagesController.php:19
msgid "Pigis"
msgstr "Pigis"

#: Controller/PagesController.php:79
msgid "Admin"
msgstr "Admin"

#: Controller/UsersController.php:16;22
msgid "Login"
msgstr "Login"

#: Controller/UsersController.php:17
msgid "Prihláste sa do systému"
msgstr "Log into system"

#: Controller/UsersController.php:37
msgid "Prístup zamietnutý"
msgstr "Access denied"

#: Controller/UsersController.php:48
msgid "Nastavenia"
msgstr "Settings"

#: Controller/UsersController.php:49
msgid "Zmena hesla užívateľa"
msgstr "Change user password"

#: Controller/UsersController.php:67
msgid "Nesprávne údaje"
msgstr "Wrong data"

#: Controller/UsersController.php:77
msgid "Nastavenia boli úspešne zmenené."
msgstr "Settings updated"

#: Controller/UsersController.php:81
msgid "Počas zmeny nastavní nastala bližšie nešpecifikovaná chyba. Prosím skúste sa ich zmeniť znovu."
msgstr ""

#: Controller/UsersController.php:86;91
msgid "Chyba pri validacii"
msgstr ""

#: Plugin/Upload/Console/Command/ThumbnailShell.php:34
msgid "Which model would you like to regenerate thumbnails for?"
msgstr ""

#: Plugin/Upload/Console/Command/ThumbnailShell.php:39
msgid "<error>"
msgstr ""

#: Plugin/Upload/Model/Behavior/UploadBehavior.php:1078
msgid "No filename after parsing. Function %s returned an invalid filename"
msgstr ""

#: View/Elements/daily_menu.ctp:3
#: View/Pages/admin_home.ctp:20;68
msgid "Predjedlo"
msgstr "Appetizer"

#: View/Elements/daily_menu.ctp:12
#: View/Pages/admin_home.ctp:27;75
msgid "Polievka"
msgstr "Soup"

#: View/Elements/daily_menu.ctp:21
#: View/Pages/home.ctp:65
msgid "Denná ponuka"
msgstr "Daily offer"

#: View/Pages/admin_home.ctp:120;167
msgid "Názov"
msgstr "Name"

#: View/Elements/admin/menu.ctp:23
msgid "Odhlásiť sa"
msgstr ""

#: View/Elements/admin/menu.ctp:47
msgid "Prehľad voucherov"
msgstr ""

#: View/Elements/admin/menu.ctp:52;72
msgid "Pridať nový"
msgstr ""

#: View/Elements/admin/menu.ctp:57
msgid "Záznamy o rozposielaní"
msgstr ""

#: View/Elements/admin/menu.ctp:67
msgid "Prehľad produktov"
msgstr ""

#: View/Elements/admin/menu.ctp:82
msgid "Prehľad kódov"
msgstr ""

#: View/Elements/admin/menu.ctp:87
msgid "Pridať nové"
msgstr ""

#: View/Pages/admin_home.ctp:34;82
msgid "Hlavné jedlo"
msgstr ""

#: View/Pages/home.ctp:3
msgid "Toto je Pigis"
msgstr "This is Pigis"

#: View/Pages/home.ctp:4
msgid "lokálne bistro"
msgstr "local bistro"

#: View/Pages/home.ctp:14
msgid "Zaručujem kvalitu surovín od lokálnych farmárov."
msgstr "I guarantee the quality of ingredients from local farmers."

#: View/Pages/home.ctp:15
msgid "šéfkuchár"
msgstr "chef"

#: View/Pages/home.ctp:26
msgid "To najlepšie z Bystrici"
msgstr "The best from Bistrica"

#: View/Pages/home.ctp:27
msgid "100% čerstvé suroviny od lokálnych farmárov"
msgstr "100% fresh ingredients from local farmers"

#: View/Pages/home.ctp:28
msgid "Naše bistro Pigis je výnimočné hlavne zameraním sa na sezónnosť, poctivosť a domáce produkty. Každý deň spolupracujeme s lokálnymi farmármi a pestovateľmi. Poznáme ich osobne a preto zaručujeme kvalitu ich produktov."
msgstr "Our bistro Pigis is exceptional in focus on seasonality, honesty and homemade products. Every day we work with local farmers. We know each other personally and therefore we guarantee the quality of their products."

#: View/Pages/home.ctp:29
msgid "Ak máte záujem o rezerváciu rodinnej oslavy či business stretnutia v našej reštaurácii, radi vám pripravíme aj jedlá, či degustačné menu, ktoré nie sú súčasťou aktuálnej ponuky."
msgstr "If you are interested in booking family reunions or business meetings in our bistro, we can prepare meals or tasting menu that are not part of actual offer."

#: View/Pages/home.ctp:40
msgid "Pečieme<br>domáci chlieb"
msgstr "We offer<br>home-baked bread"

#: View/Pages/home.ctp:43
msgid "Pripravujeme<br>vlastné koláče"
msgstr "We prepare<br>homemade desserts"

#: View/Pages/home.ctp:46
msgid "Melieme kávu<br>z lokálnej pražiarne"
msgstr "We grind coffee<br>from local roasters"

#: View/Pages/home.ctp:49
msgid "Podávame<br>domáce zemiačky"
msgstr "We serve<br>homemade fries"

#: View/Pages/home.ctp:58
msgid "Denné menu<br> a sezónne špeciality"
msgstr "Daily offer<br> and seasonal specials"

#: View/Pages/home.ctp:66
msgid "pracovné dni od 11.00 do 14.30"
msgstr "working days from 11.00 to 14.30"

#: View/Pages/home.ctp:79
msgid "pracovné dni od 15.30 do 21.00,<br>víkend od 11.30 do 21.00"
msgstr "working days from 15.30 to 21.00,<br>weekends from 11.30 to 21.00"

#: View/Pages/home.ctp:93
msgid "Dostávajte dennú ponuku na svoj e-mail"
msgstr "Sign up for our daily offers"

#: View/Pages/home.ctp:119
msgid "Ručná výroba makróniek a koláčov"
msgstr "Handmade macarons and cakes"

#: View/Pages/home.ctp:122
msgid "Candy bar"
msgstr "Candy bar"

#: View/Pages/home.ctp:123
msgid "Svadba, oslava, event... tam všade nesmú chýbať naše makrónky a koláčiky."
msgstr "Wedding, party, event... you shouldn’t miss our macarons and cakes there!"

#: View/Pages/home.ctp:124
msgid "Pripravíme pre vás krásny stolík s dobrotami, ktorý zladíme s vašou výzdobou."
msgstr "We will prepare a beautiful sweet table with goodies that will fit with your decor."

#: View/Pages/home.ctp:128
msgid "Makrónky a koláče"
msgstr "Macarons and cakes"

#: View/Pages/home.ctp:129
msgid "Ponúkame vlastnoručne vyrábané makrónky a koláče najrôznejších delikátnych chutí."
msgstr "We offer handmade macarons and cakes. They are all made of delicate flavours."

#: View/Pages/home.ctp:130
msgid "Môžete ich ochutnať v bistre Pigis pri kávičke alebo vám ich pripravíme na domácu oslavu."
msgstr "You may savor them with fresh coffee in Pigis bistro or we can prepare them as take away."

#: View/Pages/home.ctp:152
msgid "Adresa"
msgstr "Address"

#: View/Pages/home.ctp:156
msgid "Rezervácie"
msgstr "Reservations"

#: View/Pages/home.ctp:163
msgid "Otváracie hodiny"
msgstr "Opening hours"

#: View/Pages/home.ctp:166
msgid "Pondelok"
msgstr "Monday"

#: View/Pages/home.ctp:170
msgid "Utorok"
msgstr "Tuesday"

#: View/Pages/home.ctp:174
msgid "Streda"
msgstr "Wednesday"

#: View/Pages/home.ctp:178
msgid "Štvrtok"
msgstr "Thursday"

#: View/Pages/home.ctp:182
msgid "Piatok"
msgstr "Friday"

#: View/Pages/home.ctp:186
msgid "Sobota"
msgstr "Saturday"

#: View/Pages/home.ctp:190
msgid "Nedeľa"
msgstr "Sunday"

#: View/Pages/home.ctp:194;251
msgid "V čase od 14.30 do 15.30<br>je kuchyňa zatvorená."
msgstr "Our kitchen is closed on<br>working days from 14.30 to 15.30."

#: View/Pages/home.ctp:200
msgid "Sociálne médiá"
msgstr "Social media"

#: View/Users/admin_login.ctp:8
msgid "User"
msgstr "User"

#: View/Users/admin_login.ctp:16
msgid "Heslo"
msgstr "Password"

#: View/Users/admin_login.ctp:21
msgid "Prihlásiť sa"
msgstr "Log in"

#: View/Users/admin_settings.ctp:9
msgid "Aktuálne heslo"
msgstr "Actual password"

#: View/Users/admin_settings.ctp:15
msgid "Nové heslo"
msgstr "New password"

#: View/Users/admin_settings.ctp:19
msgid "Heslo nesmie mať menej ako 8 znakov a viac ako 20 znakov."
msgstr ""

#: View/Users/admin_settings.ctp:20
msgid "Heslo a overenie hesla sa musia zhodovať."
msgstr ""

#: View/Users/admin_settings.ctp:27
msgid "Overenie hesla"
msgstr ""

#: View/Users/admin_settings.ctp:31
msgid "Uložiť zmeny"
msgstr ""

#: Model/User.php:validation for field pass;validation for field password
msgid "Slabé heslo"
msgstr ""

#: Model/User.php:validation for field pass
msgid "between"
msgstr ""

#: Model/User.php:validation for field pass
msgid "compare"
msgstr ""

#: Model/User.php:validation for field username
msgid "A username is required"
msgstr ""

msgid "Úvod"
msgstr "Home"

msgid "Denné menu"
msgstr "Daily offer"

msgid "Špeciality"
msgstr "Specials"

msgid "Akcie"
msgstr "Events"

msgid "Fotogaléria"
msgstr "Gallery"

msgid "Makrónkovo"
msgstr "Makrónkovo"

msgid "Kontakt"
msgstr "Contact"

msgid "Odoslať"
msgstr "Send"

msgid "váš e-mail"
msgstr "your e-mail"
