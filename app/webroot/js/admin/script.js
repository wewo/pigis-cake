$(function() {

	handleDailyOfferFormSubmit();
	handleSpecialsFormSubmit();
	handleEventsFormSubmit();
	handleNewsletterFormSubmit();
	handlePanelsSlideToggle();
	handleNewsletter();
	handleSendNewsletter();
});

function handleDailyOfferFormSubmit() {
	$('.daily-menu-form').on('submit', function() {

		var _this = this;

		$.ajax({
			'url': '/pages/updateDailyOffer',
			'method': 'post',
			'data': {
				'id': $(this).find('input[name="data[DailyOffer][id]"]').val(),
				'appetizer': $(this).find('textarea[name="data[DailyOffer][appetizer]"]').val(),
				'soup': $(this).find('textarea[name="data[DailyOffer][soup]"]').val(),
				'dish': $(this).find('textarea[name="data[DailyOffer][dish]"]').val(),
				'limonade': $(this).find('textarea[name="data[DailyOffer][limonade]"]').val(),
				'dessert': $(this).find('textarea[name="data[DailyOffer][dessert]"]').val()
			},
			success: function(response) {
				$(_this).parents('.panel').addClass('green');
				setTimeout(function() {
					$(_this).parents('.panel-body').slideUp();
					$(_this).parents('.panel').removeClass('green');
				}, 500);
			}
		});

		return false;
	});
}

function handlePanelsSlideToggle() {
	$('.panel-heading').on('click', function() {
		$(this).next('.panel-body').slideToggle();
	});
}

function handleSpecialsFormSubmit() {
	$('.specials-form').on('submit', function() {

		var _this = this;
		var data = {};

		$(this).find('.specials-row').each(function(i, row) {
			data[i] = {};
			$(row).find('input').each(function(j, input) {
				data[i][$(input).data('key')] = $(input).val();
			});
		});

		data['specials_name'] = $(this).find('.specials-name').val();
		data['config-val-id'] = $(this).find('.specials-name').data('config-val-id');

		$.ajax({
			'url': '/pages/updateSpecials',
			'method': 'post',
			'data': {'data': data},
			success: function(response) {				
				$(_this).parents('.panel').addClass('green');
				setTimeout(function() {
					$(_this).parents('.panel-body').slideUp();
					$(_this).parents('.panel').removeClass('green');
				}, 500);
			}
		});

		return false;
	});
}

function handleNewsletterFormSubmit() {
	$('#newsletter-form').on('submit', function() {

		$.ajax({
			'url': '/pages/updateNewsletter',
			'method': 'post',
			'data': {
				'date': $('#NewsletterDate').val(),
				'opening_time': $('#NewsletterOpeningTime').val(),
				'image_url': $('#NewsletterImageUrl').val(),
				'image_alt': $('#NewsletterImageAlt').val(),
			},
			success: function(response) {				
				$('#newsletter-form input[type="submit"]').blur();
				$('#newsletter-form input[type="submit"]').removeClass('btn-primary').addClass('btn-success').val('Uložené');
				setTimeout(function() {
					$('#newsletter-form input[type="submit"]').removeClass('btn-success').addClass('btn-primary').val('Uložiť');
				}, 2500);

				$('#newsletter-date').text( $('#NewsletterDate').val() );
				$('#newsletter-opening-time').text( $('#NewsletterOpeningTime').val() );
				$('#newsletter-image').attr('src', $('#NewsletterImageUrl').val() );
				$('#newsletter-image').attr('alt', $('#NewsletterImageAlt').val() );				
			}
		});

		return false;
	});
}

function handleNewsletter() {
	$('#NewsletterDate').datepicker({
		'language': 'sk',
		'format': 'd. MM yyyy'
	});

	$('#NewsletterDate').next('.input-group-addon').on('click', function() {
		$('#NewsletterDate').datepicker('show');
	});
}

function handleSendNewsletter() {
	$('#send-newsletter-button').on('click', function() {
		$.ajax({
			'url': '/pages/sendNewsletter',
			'method': 'post',
			success: function(response) {
				$('#send-newsletter-button').blur();
				$('#send-newsletter-success').fadeIn();
				setTimeout(function() {
					$('#send-newsletter-success').fadeOut();
				}, 2500);
			}
		});

		return false;
	});
}

function handleEventsFormSubmit() {
	$('#EventAdminHomeForm').on('submit', function() {

		var _this = this;
		var data = {};

		$(this).find('.event-row').each(function(i, row) {
			data[i] = {};
			$(row).find('input, textarea').each(function(j, input) {
				data[i][$(input).data('key')] = $(input).val();
			});
		});

		$.ajax({
			'url': '/events/update',
			'method': 'post',
			'data': {'data': data},
			success: function(response) {
				$(_this).find('.success').fadeIn();
				setTimeout(function() {
					$(_this).find('.success').fadeOut();
				}, 1000);
			}
		});

		return false;
	});
}