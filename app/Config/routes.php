<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'Pages', 'action' => 'home'));
	Router::connect('/admin',			array('controller' => 'Pages',	'action' => 'home',		'admin'=>true));
	Router::connect('/admin/newsletter', array('controller' => 'Pages',	'action' => 'newsletter','admin'=>true));
	Router::connect('/admin/events',		array('controller' => 'Events',	'action' => 'home',	'admin'=>true));
	Router::connect('/admin/login',		array('controller' => 'Users',	'action' => 'login',	'admin'=>true));
	Router::connect('/admin/logout',	array('controller' => 'Users',	'action' => 'logout',	'admin'=>true));
	Router::connect('/admin/settings',	array('controller' => 'Users',	'action' => 'settings',	'admin'=>true));

	Router::connect('/sk',	array('controller' => 'Pages',	'action' => 'changeLanguage', 'sk'));
	Router::connect('/en',	array('controller' => 'Pages',	'action' => 'changeLanguage', 'en'));

	Router::connect('/sign-up',	array('controller' => 'NewsletterAddresses',	'action' => 'singUp'));
	Router::connect('/preview-email', array('controller' => 'Pages', 'action' => 'previewEmail'));
	Router::connect('/unsubscribe/*', array('controller' => 'NewsletterAddresses', 'action' => 'unsubscribe'));

	Router::connect('/38907ae5314b705',	array('controller' => 'Pages',	'action' => 'hideDailyOffer'));


/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
