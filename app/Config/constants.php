<?php 

define( 'HOST', (strpos($_SERVER['SERVER_NAME'], 'localhost') !== false) ? 'mysql51.websupport.sk:3309' : 'localhost' );

// flash box typy
define('FLASH_OK', 'messages/flash-ok');
define('FLASH_ERROR', 'messages/flash-error');
define('FLASH_WARNING', 'messages/flash-warning');
define('FLASH_INFO', 'messages/flash-info');

define('SK_DAILY_OFFER_ID', 1);
define('EN_DAILY_OFFER_ID', 2);

?>
